簡易的な説明書き

■ライセンスについて
MITライセンスです。

■ファイル及びフォルダについて
/animation_seq/
缶が開いて泡が出るまでのアニメーションのシーケンス画像が含まれるフォルダです。

/dev/
index.htmlやenchant.jsが含まれた開発用フォルダです。
9leapへのアップロード時にはこのフォルダをzip圧縮してアップロードしています。

/append_animation.sh
animation_seqフォルダにあるシーケンス画像を連結するためのシェルスクリプトです
と言っても中身で実行しているのは1行だけなので、手作業でも何とかなります。
ImageMagickを利用しています。

/bubble.m4a
ビールを注いだ音を録音したファイルです。

/bubble.wav
缶を開ける音とビールを注いだ音をつなげた音声ファイルです。
ファイル名がややこしいですが色々あってこうなりました

/can_tex.ai
缶の3Dモデルに利用した法線マップの作成に利用したAdobe Illustratorファイルです。

/can_tex.png
缶の3Dモデルに利用した法線マップテクスチャです

/fluid_can.blend
実際に缶と泡のアニメーション作成利用したblenderファイルです。

/fluid_test.blend
実験用に作成していたblenderファイルです

/opencan.m4a
缶を開ける音を録音したファイルです。