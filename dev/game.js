enchant();

//無限
var mugen = false;

//スコア
var speed = 0;
//状態遷移用スイッチ群
var can_animation = false;
var timer_count = false;
var device_motion = false;

//缶
var open_can_audio = new Audio( "opencan.mp3" );
open_can_audio.load();
var open_can = false;

//泡
var bubble_audio = new Audio( "bubble.mp3" );
bubble_audio.load();

//z軸処理用の色々
var z_motion = null;
var z_delta;
var z_motion_buffer;

//シェイク開始条件/終了条件
var z_shake_begin_threshould = 9;
var z_shake_end_threshould = 4;
var z_max_delta;
var z_shaking = false;
window.onload = function() {
    var Rectangle = enchant.Class.create({
        initialize: function(x, y, width, height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        },
        right: {
            get: function() {
                return this.x + this.width;
            }
        },
        bottom: {
            get: function() {
                return this.y + this.height;
            }
        }
    });
    var game = new Game(320, 320);
    game.fps = 30;
    game.preload('animation.png');
    game.onload = function() {
		//タイトルというか説明
		var title = new Label('');
		title.color="#FFF";
		title.y = 8;
		title.addEventListener( 'enterframe' , function() {
			//プレイできない場合
        	if ( z_motion == undefined )
			{
				if ( mugen )
				{
						this.text = '<div style="text-align:center"><strong>画面上下方向に</strong>指(マウス)でこすり続けてください、<br />指(マウス)を離すと缶が空きます</div>'
				}
				else
				{
					this.text = '<div style="text-align:center"><strong>画面の上下方向に</strong><br />10秒間よく指(マウス)でこすり続けてください</div>'
				}
			}
			else
			{
				if ( device_motion )
				{
					if ( mugen )
					{
						this.text = '<div style="text-align:center"><strong>缶の上下方向に</strong><br />よく振ってから缶を開けてください</div>'
					}
					else
					{
						this.text = '<div style="text-align:center"><strong>缶の上下方向に</strong>10秒間よく振ってください</div>'
					}
				}
				can.addEventListener('touchbegin', openCan);
				this.removeEventListener( 'enterframe', arguments.callee, false);
			}
		});
		//カウンター
		var timer = new Label();
		timer.color="#FFF";
		timer.y = 270;
		if ( mugen )
		{
			timer.ms = 0;
		}
		else
		{
			timer.ms = 10000;
		}
		timer.date_object = new Date();
		timer.time_buffer = timer.date_object.getTime();
		timer.text_prefix = '<div style="text-align:center;font-size:32px">';
		timer.text_suffix = '</div>';
		timer.text = timer.text_prefix + msToSecondText( timer.ms ) + timer.text_suffix;
		timer.addEventListener( 'enterframe', function() {
			this.date_object = new Date();
			if ( timer_count )
			{
				if ( mugen )
				{
				//無限なのでカウントアップ
					this.ms += ( this.date_object.getTime() - timer.time_buffer );
					this.text = this.text_prefix + msToSecondText( this.ms ) + this.text_suffix;
				}
				else
				{
					this.ms = this.ms - ( this.date_object.getTime() - timer.time_buffer );
					if ( this.ms < 3000 )
					{
						this.visible = !this.visible;
					}
					if ( this.ms <= 0 )
					{
						//時間切れ
						this.text = this.text_prefix + '00.00' + this.text_suffix;
						device_motion = false;
						window.removeEventListener('devicemotion', arguments.callee);
						can.removeEventListener('touchmove', arguments.callee)
						can.y = 0;
						if ( this.ms < -2000 )
						{
							this.visible = true;
							timer_count = false;
							can_animation = true;
							bubble_audio.play();
							speed = parseInt(Math.round( speed ));
						}
					}
					else
					{
						this.text = this.text_prefix + msToSecondText( this.ms ) + this.text_suffix;
					}
				}
			}
			this.time_buffer = this.date_object.getTime();
		});
		var can = new Sprite(320,320);
		can.x = 0;
		can.y = 0;
		can.endframe = 17;
		can.image = game.assets['animation.png'];
		can.addEventListener( 'enterframe', function() {
			if ( can_animation )
			{
				can.y = 0;
			//アニメーション
				if ( this.frame != this.endframe)
				{
					this.frame = ++this.frame;
				}
				else
				{
					//結果
					if ( mugen )
					{
						game.end( parseInt(speed), "缶を" + msToSecondText(timer.ms) + "秒間振り続け、泡の瞬間到達噴出速度は " + parseInt(speed) + "km/h に達しました" );
						//alert(msToSecondText(timer.ms) + "秒振り続け、瞬間到達噴出速度は " + parseInt(speed) + "km/h に達しました");
					}
					else
					{
						if ( open_can && timer.ms > 0 )
						{
							game.end( parseInt(speed), "瞬間到達噴出速度: " + parseInt(speed) + "km/h (" + msToSecondText(timer.ms) + "秒残し)" );
						}
						else
						{
							game.end( parseInt(speed), "瞬間到達噴出速度: " + parseInt(speed) + "km/h" ); 
						}
					}
				}
				if ( this.frame >= 3 )
				{
					speed_meter.text = '<div style="text-align:center;font-weight:bold">' +Math.round( speed * (this.frame / this.endframe ) ) + ' km/h</div>';
				}
			}
		});
		can.addEventListener('touchbegin', function() {
			z_motion_buffer = 0;
		});
		can.addEventListener('touchmove', function(e) {
			zMotionDetect( e.y*5 );
		});
		can.addEventListener('touchend', openCan);
		//缶を開ける関数
		function openCan() {
			//ふらずに開ける
			if ( !timer_count )
			{
				if ( !can_animation && !open_can)
				{
					this.frame = 1;
					open_can_audio.play();
					//bubble_audio.play();
					setTimeout( function() {game.end( 0, "缶を振らずに開けました" )}, 500 ); 
				}
			}
			//振ってる時に開ける
			else
			{
				timer_count = false;
				//can_animation = true;
				open_can = true;
				speed = parseInt(Math.round( speed ));
				bubble_audio.play();
				setTimeout( function() { can_animation = true; }, 330);
			}
		}
		/*var z_debug = new Label();
		z_debug.color = "#FFF";
		z_debug.addEventListener( 'enterframe', function() {
			this.text = "" + speed;
		});
		game.rootScene.addChild(z_debug);*/
		var speed_meter = new Label();
		speed_meter.color = "#000";
		speed_meter.y = 256;
		
		game.rootScene.addChild(title);
		game.rootScene.addChild(timer);
        game.rootScene.addChild(can);
        game.rootScene.addChild(speed_meter);
        game.rootScene.backgroundColor = 'rgb(0, 0, 0)';
        
		//加速度センサー処理
		window.addEventListener("devicemotion", function (e) {
			device_motion = true;
			zMotionDetect(e.accelerationIncludingGravity.z);
		});
		function zMotionDetect( z_acceleration ) {
			z_motion = z_acceleration;
			z_delta = Math.abs(z_motion - z_motion_buffer);
			z_delta_noabs = z_motion - z_motion_buffer;
			if ( z_shaking )
			{
				if ( z_delta > z_max_delta)
				{
					z_max_delta = z_delta;
					if ( device_motion )
					{
						can.y += z_delta_noabs/2;
					}
					else
					{
						can.y += z_delta_noabs/20;
					}
				}
				else
				{
					can.y = 0;
				}
				if ( z_delta < z_shake_end_threshould )
				{
				//勢いがなくなったら
					speed += z_max_delta/20;
					z_shaking = false;
				}
				else if ( (z_motion > 0 && z_motion_buffer < 0) || ( z_motion < 0 && z_motion_buffer > 0) )
				{
				//勢いのまま反転したら
					speed += z_max_delta/10;
					z_max_delta = z_delta;
				}
			}
			else
			{
				if ( z_delta > z_shake_begin_threshould )
				{
					z_shaking = true;
					z_max_delta = z_delta;
					if ( !timer_count )
					{
						timer_count = true;
					}
				}
				can.y = 0;
			}
			z_motion_buffer = z_motion;
		}
    };
    game.start();
};

function msToSecondText( ms )
{
	return ""+ parseInt(ms/10000) + parseInt((ms%10000)/1000) + '.' + parseInt((ms%1000)/100) + parseInt((ms%100)/10);
}